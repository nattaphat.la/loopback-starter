FROM node:18-alpine
RUN apk add git
WORKDIR /app
COPY patches package.json yarn.lock /app/
RUN yarn
COPY . /app/
CMD ["yarn", "start"]