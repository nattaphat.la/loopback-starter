'use strict';

const app = require('../server');

const roles = [
  {
    name: 'admin',
  },
];

const seedRoles = async () => {
  const {
    Role,
  } = app.models;
  try {
    console.debug('Seed Role');
    const result = await Promise.all(roles.map((role) => {
      return Role.findOrCreate({
        where: {
          name: role.name,
        },
      }, role);
    }));
    console.debug('Seed Role success');
    return Promise.resolve(result);
  } catch (e) {
    console.error('Seed Role fail', e);
    throw e;
  }
};

module.exports = {
  roles,
  seedRoles,
};
