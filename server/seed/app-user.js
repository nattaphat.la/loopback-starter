'use strict';
const app = require('../server');
const users = [
  {
    name: process.env.ADMIN_NAME || 'admin',
    username: process.env.ADMIN_USERNAME || 'admin',
    password: process.env.ADMIN_PASSWORD || 'password',
    email: process.env.ADMIN_EMAIL || 'admin@yopmail.com',
    role: 'admin',
    emailVerified: true,
  },
];
const seedUser = async () => {
  const {
    AppUser,
    Role,
    RoleMapping,
  } = app.models;
  try {
    console.debug('Seed User');
    const result = await Promise.all(users.map(async (userInfo) => {
      const [user] = await AppUser.findOrCreate({
        where: {
          or: [
            {
              username: userInfo.username,
            },
            {
              email: userInfo.email,
            },
          ],
        },
      }, userInfo);
      console.debug('seed user success', userInfo.username);
      // assign role
      if (userInfo.role) {
        // find role by name
        const role = await Role.findOne({
          where: {
            name: userInfo.role,
          },
        });
        const [roleMapping] = await RoleMapping.findOrCreate({
          where: {
            principalType: RoleMapping.USER,
            principalId: user.id,
            roleId: role.id,
          },
        }, {
          principalType: RoleMapping.USER,
          principalId: user.id,
          roleId: role.id,
        });
        console.debug(`Assign user ${user.id} to role ${role.id} with role mapping ${roleMapping.id}`);
      }
      return user;
    }));
    console.log('Seed User success');

    return Promise.resolve(result);
  } catch (e) {
    console.log('Seed user fail', e);
    throw e;
  }
};
module.exports = {
  seedUser,
  users,
};
