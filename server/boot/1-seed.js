const {seedUser} = require('../seed/app-user');
const {seedRoles} = require('../seed/role');

module.exports = async (app) => {
  console.debug('Seed Data');
  try {
    await seedRoles();
    await seedUser();
    console.debug('Seed Data success');
    return Promise.resolve();
  } catch (e) {
    console.error('Seed fail');
    throw new Error('Seed - Error occur');
  }
};
