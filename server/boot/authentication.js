// Copyright IBM Corp. 2016,2019. All Rights Reserved.
// Node module: loopback-workspace
// This file is licensed under the MIT License.
// License text available at https://opensource.org/licenses/MIT

'use strict';
const loopback = require('loopback')

module.exports = function enableAuthentication(server) {
  // enable authentication
  server.use(loopback.token({
    // model: server.models.AccessToken,
    currentUserLiteral: 'me',
  }));
  server.enableAuth();
};
