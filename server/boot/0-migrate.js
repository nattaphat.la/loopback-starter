'use strict';
/**
 * 
 * @param {import("loopback").LoopBackApplication} server 
 */
module.exports = async function migrate(server) {
  // get datasource
  try {
    const ds = server.dataSources.db
    console.debug('Start migration')
    await new Promise((resolve, reject) => {
      console.debug('Connecting Database')
      ds.on('connected', () => resolve());
    });
    console.debug("Database connected")
    const isActual = await new Promise((resolve, reject) => {
      ds.isActual(function (err, actual) {
        if (err) {
          console.error('Cannot check actual', err);
          return reject(err);
        }
        console.debug('datbase isActual', actual);
        return resolve(actual);
      });
    });
    if (isActual) {
      console.debug('Schema up to date - no migration needed')
      return Promise.resolve()
    }
    // not actual
    await new Promise((resolve, reject) => {
      ds.autoupdate((err) => {
        if (err) {
          console.error('Migrate error', err);
          return reject(err);
        }
        console.debug('Migrate success');
        return resolve();
      });
    });
    return Promise.resolve()
  } catch (e) {
    console.error('Migration error', e)
    return Promise.reject(e)
  }

};
