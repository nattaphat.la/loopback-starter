'use strict';

module.exports = function(Appuser) {
  Appuser.on('resetPasswordRequest', function(info) {
    const url = `${process.env.FRONTEND_URL}/${process.env.RESET_PASSWORD_PATH || 'reset-password'}`;
    const html = `Click <a href="${url}?access_token=${info.accessToken.id}">here</a> to reset your password`
    Appuser.app.models.Email.send({
      to: info.email,
      from: process.env.SENDER_EMAIL,
      subject: 'Password reset',
      html: html
    }, function(err) {
      if (err) return console.log('> error sending password reset email');
      console.log('> sending password reset email to:', info.email);
    });
  });
};
